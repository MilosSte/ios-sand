//
//  UIStylingProtocols.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

//setup views inside viewcontroller
protocol UIViewStyling {
    func setupViews()
}
//setup constraints inside viewcontroller
protocol UIConstraintStyling{
    func setupConstraints()
}

protocol UIStyleCollectionView {
    func configure()
}

protocol UIStyling : UIViewStyling, UIConstraintStyling{}
