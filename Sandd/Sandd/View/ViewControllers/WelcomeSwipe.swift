//
//  WelcomeSwipe.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import UIKit

class WelcomeSwipe: UIViewController {
    lazy var viewModel : ViewModelWelcomeSwipe = {
        let viewModel = ViewModelWelcomeSwipe(superView:self.view)
        return viewModel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupWelcomeSwipeViewController()
        viewModel.addSwipeIconAndLabel()
    }

}
