//
//  RegisterStepOneViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import UIKit

class RegisterStepOneViewController: UIViewController {

    lazy var viewModel : ViewModelRegisterStepOne = {
        let viewModel = ViewModelRegisterStepOne(superview:self.view)
        return viewModel
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor(r: 0, g: 60, b: 152, alpha: 1)
        viewModel.setupViews()
        // Do any additional setup after loading the view.
    }

}
