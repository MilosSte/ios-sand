//
//  LoginViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    lazy var viewModel : ViewModelWelcomeSwipe = {
        let viewModel = ViewModelWelcomeSwipe(superView:self.view)
        return viewModel
    }()
    
    lazy var viewModelLogin : ViewModelLoginViewController = {
        let viewModel = ViewModelLoginViewController(superview: self.view,viewController:self)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.setupLoginViewController()
        self.viewModelLogin.setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

}
