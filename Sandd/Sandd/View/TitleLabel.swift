//
//  UITitleLabelLayer.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Setup for TitleLabel
//this label is placed as title text inside TitleView.
class TitleLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        setupConstraints()
    }
}

extension TitleLabel : UIStyling {
    func setupViews() {
        print("setup view label")
        self.backgroundColor = UIColor(r: 0, g: 60, b: 152, alpha: 1)
        self.textColor = .white
        self.font = UIFont.custom(type: .bold, size: 32)
    }
    
    func setupConstraints() {
        print(" setup constraints label")
    }
}

//MARK:- Setup TitleView below navigation
// this view is added below with text we use in graphics for title
//title is set in Title Label
class TitleView : UIView {
    var titleLabel: TitleLabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        setupViews()
        setupConstraints()
    }
    //MARK: Setup text of title
    func setupTitle(title:String?) {
        if let _title = title {
            let stringPartToColor = "."
            let completeTitle = _title + stringPartToColor
            let range = (completeTitle as NSString).range(of: stringPartToColor)
            let attribute = NSMutableAttributedString.init(string: completeTitle)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
            
            
            self.titleLabel.attributedText = attribute
        }
    }
}

extension TitleView:UIStyling {
    func setupViews() {
        print("setup...")
        self.backgroundColor = UIColor(r: 0, g: 60, b: 152, alpha: 1)
        let frame = CGRect(x: 20, y: 0, width:self.frame.width - 20, height: self.frame.height - 12)
        titleLabel = TitleLabel(frame: frame)
        titleLabel.text = " Registreren"
        self.addSubview(titleLabel)
        setupConstraints()
    }
    
    func setupConstraints() {
        print("constraints...")
    }
}
