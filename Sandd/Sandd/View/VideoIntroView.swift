//
//  VideoIntroView.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class VideoIntroView: UIView {
    var screenFrame       : CGRect!
    var backgroundLayer   : ImageLayer!
     var _videoLayer      : VideoLayer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
        setupConstraints()
    }
}
extension VideoIntroView : UIStyling {
    func setupViews() {
        //background layer
        guard let backgroundLayer = ImageLayer(superView: self, image: #imageLiteral(resourceName: "background").cgImage,opacity:0.86)else{return}
        self.backgroundLayer = backgroundLayer
        layer.addSublayer(backgroundLayer)
        screenFrame = backgroundLayer.frame
        
        //add video
        let videoURL = Bundle.main.url(forResource: "video_intro", withExtension: "m4v")
        guard let videoLayer = VideoLayer(superView: self, videoURL: videoURL) else { return }
        _videoLayer = videoLayer
        layer.addSublayer(_videoLayer.backgroundVideoLayer)
        videoLayer.startPlaying()
    }
    
    func setupConstraints() {
       
    }
}
