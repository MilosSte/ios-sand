//
//  ImageLayer.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import UIKit
import Foundation

class ImageLayer : CALayer {
    
    init?(superView : UIView,image : CGImage?,opacity: Float = 1.0){
        super.init()
        
        guard let imageCg = image else { print("ImageDoesn'tExist! \(#function)")
            return nil }
        contents        = imageCg
        frame           = superView.bounds
        contentsScale   = UIScreen.main.scale
        shouldRasterize = true
        self.opacity    = opacity
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
