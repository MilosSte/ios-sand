//
//  VideoLayer.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import AVFoundation
import UIKit

private enum VideoErrors : String, Error{
    case VideoDoesntExist = "Video doesn't exist . Check path to video"
}
class VideoLayer {
    private var _opacity              : Float = 0.8
    private var player                : AVQueuePlayer!
    private var playerLoop            : AVPlayerLooper!
    private var _currentMediaTimePlayback : (currentTime : Double,duration : Int32)?
    var backgroundVideoLayer          : AVPlayerLayer!
    
    var opacity : Float {
        get {
            return _opacity
        }set{
            _opacity = newValue >= 0 && newValue <= 1.0 ? newValue : 1.0
        }
    }
    var currentMediaTimePlayback : (currentTime : Double,duration : Int32)? {
        get {
            if  let currentTime = player.currentItem?.currentTime().seconds , let duration = player.currentItem?.asset.duration.timescale {
                _currentMediaTimePlayback = (currentTime,duration)
                return _currentMediaTimePlayback
            }
            return nil
        }set {
            if let _mediaTime = newValue {
                player.currentItem?.seek(to: CMTimeMakeWithSeconds(_mediaTime.currentTime, _mediaTime.duration), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                //player.play()
            }
            _currentMediaTimePlayback = newValue
        }
    }
    init?(superView : UIView,videoURL : URL?) {
        guard let _videoPath = videoURL else  {
            print(VideoErrors.VideoDoesntExist.localizedDescription)
            return nil
        }
        loadVideoIntro(superView: superView, videoURL: _videoPath)
    }
    private func loadVideoIntro(superView: UIView,videoURL : URL){
        player = AVQueuePlayer()
        player.externalPlaybackVideoGravity = AVLayerVideoGravity.resizeAspectFill
        let playerItem = AVPlayerItem(url: videoURL)
        playerLoop     = AVPlayerLooper(player: player, templateItem: playerItem)
        player.insert(playerItem, after: nil)
        
        backgroundVideoLayer         = AVPlayerLayer(player: player)
        backgroundVideoLayer.frame   = superView.bounds
        backgroundVideoLayer.opacity = opacity
        backgroundVideoLayer.videoGravity = .resizeAspectFill
    }
    func stopPlaying(){
        if player != nil {
            player.pause()
            player.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    func startPlaying(){
        guard let _player = player else { return }
        _player.play()
    }
    required init?(coder aDecoder: NSCoder) {
        debugPrint("init(coder:) has not been implemented")
    }
}
