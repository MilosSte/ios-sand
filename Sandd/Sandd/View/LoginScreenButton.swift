//
//  LoginScreenButton.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit

class LoginButtonLoginVC: UIButton {
    var onLoginButtonClick    : (()->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
        super.touchesBegan(touches, with: event)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //add title and background so we dont need to make two same buttons from scrach
    func changeTitleAndBackgroundColor(color:UIColor?,title:String?) {
        if color != nil {
            self.backgroundColor = color!
        }
        if title != nil {
            self.setTitle(title!, for: .normal)
        }
    }
}

extension LoginButtonLoginVC {
    @objc private func clickOnButton() {
        onLoginButtonClick?()
    }
}
extension LoginButtonLoginVC : UIViewStyling {
    func setupViews() {
        setTitle("Log in", for: .normal)
        layer.cornerRadius = 32
        backgroundColor    = UIColor(r: 230, g: 0, b: 110, alpha: 1)
        titleLabel?.font   = UIFont.custom(type: .light, size: 16.0)
        addTarget(self, action: #selector(clickOnButton), for: .touchUpInside)
    }
}
