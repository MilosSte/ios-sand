//
//  ImageViewLogo.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/15/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit

 class ImageViewLogo: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setImage(imageName:String?)-> UIImageView {
        let imageView = UIImageView()
        if let _imageName = imageName {
            imageView.image = UIImage(named: _imageName)
        }
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }
}

