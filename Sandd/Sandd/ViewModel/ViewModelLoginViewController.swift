//
//  ViewModelLoginViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit

class ViewModelLoginViewController {
    let superview   : UIView!
    var btnLogin    :LoginButtonLoginVC!
    var btnRegister : LoginButtonLoginVC!
    var viewcontroller : LoginViewController!
    
    init(superview:UIView,viewController:LoginViewController) {
        self.superview = superview
        self.viewcontroller = viewController
        setupViews()
    }

    //MARK:- Action login
    //FIXME: NEED TO ADD LOGIN LOGIC
    func login(){
        print("Try login...")
    viewcontroller.viewModel.videoBackgroundView._videoLayer.stopPlaying()
    }
    //MARK:- Action register
    //FIXME: NEED TO ADD REGISTER LOGIC
    func register(){
        print("Try register...")
        viewcontroller.viewModel.videoBackgroundView._videoLayer.stopPlaying()
        //viewcontroller.viewModel.removeVideoView()
    }
}

//MARK:- Graphical elements setup for LoginViewController
extension ViewModelLoginViewController : UIStyling {
    func setupViews() {
        var frame = CGRect()
        if #available(iOS 11.0, *) {
            let safeScreenHeight = superview.safeAreaLayoutGuide.layoutFrame.size.height
             frame = CGRect(x: 20, y: safeScreenHeight - 104, width:  superview.frame.width - 32, height: 64)
        } else {
            frame = CGRect(x: 20, y: superview.frame.size.height - 84, width:  superview.frame.width - 32, height: 64)
        }
        btnLogin = LoginButtonLoginVC(frame: frame)
        superview.addSubview(btnLogin)        
        btnLogin.onLoginButtonClick = { [unowned self] in
            self.login()
        }
        
        frame = CGRect(x: 20, y: btnLogin.frame.origin.y - 80, width: btnLogin.frame.width, height: btnLogin.frame.height)
        btnRegister = LoginButtonLoginVC(frame: frame)
        superview.addSubview(btnRegister)
        btnRegister.changeTitleAndBackgroundColor(color: UIColor(r: 152, g: 161, b: 178, alpha: 1), title: "Registern")
        btnRegister.onLoginButtonClick = { [unowned self] in
            self.register()
        }
        
    }
    
    func setupConstraints() {
        print("constraints..")
    }
}
