//
//  ViewModelWelcomeSwipe.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//


import UIKit
class ViewModelWelcomeSwipe {
    let superview   : UIView!
    var imageLogo   = UIImageView()
    var screenFrame : CGRect!
    var videoBackgroundView:VideoIntroView!
    
    init(superView:UIView) {
        self.superview = superView
    }
    
    //MARK:- Add video view as background
    func addVideoView(){
        videoBackgroundView = VideoIntroView(frame: superview.frame)
        superview.addSubview(videoBackgroundView)
        //add logo and button
        setupViews()
    }
    
    //MARK: Remove video view from superview
    func removeVideoView(){
        self.videoBackgroundView._videoLayer.stopPlaying()
        self.videoBackgroundView.removeFromSuperview()
    }
    
    //MARK:- Adding gesture recognizer to view so user can move to login screen.
    func setupWelcomeSwipeViewController(){
        self.setupGesture(view: superview)
        self.addVideoView()
        addLogo()
        setupConstraints()
    }
    private func setupGesture(view : UIView){
        let swipeGesture =  UISwipeGestureRecognizer(target: self, action: #selector(runSwipeTransition))
        swipeGesture.direction  = .left
        swipeGesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(swipeGesture)
    }
    //MARK:- Adding video and background to LoginViewController screen
    func setupLoginViewController(){
        self.addVideoView()
        addLogo()
    }

    //MARK: Add logo to screen
    func addLogo(){
        self.screenFrame = superview.frame
        //logo layer
        imageLogo.image = UIImage(named: "logo")
        imageLogo.contentMode = .scaleAspectFit
        imageLogo.clipsToBounds = true
        self.superview.addSubview(imageLogo)
    }
    
    //FIXME: Need to get font for this
    //MARK: Add text and image for 1. Welcome - swipe screen
    func addSwipeIconAndLabel(){
        let label = UILabel()
        label.text = "Swipe naar links om te beginnen"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .white
        label.textAlignment = .center
        label.frame = CGRect(x: 16, y: screenFrame.height - 87, width: screenFrame.width - 32, height: 17)
        superview.addSubview(label)
        
        let swipeImage = UIImageView()
        swipeImage.contentMode = .scaleAspectFit
        swipeImage.image = UIImage(named: "swipeLeft")
        swipeImage.frame = CGRect(x: screenFrame.width/2 - 25, y: screenFrame.height - 155, width: 50, height: 50)
        superview.addSubview(swipeImage)
    }
    
    //action for gesture recognizer
    @objc func runSwipeTransition() {
        self.removeVideoView()
        appDelegate.navigateTo(viewController:LoginViewController())
    }
}

extension ViewModelWelcomeSwipe:UIStyling {
    func setupViews() {
    }
    
    //MARK:- Setup position of elements
    func setupConstraints() {
            // position of logo
            imageLogo.frame = CGRect(x: (screenFrame.width - screenFrame.width * 0.632)/2, y: screenFrame.height * 0.388, width: screenFrame.width * 0.632, height: screenFrame.width * 0.632 * 0.42194)
    }
}
