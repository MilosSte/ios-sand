//
//  ViewModelRegisterStepOne.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import UIKit
class ViewModelRegisterStepOne {
    let superview : UIView!
    init(superview:UIView) {
        self.superview = superview
    }
}

extension ViewModelRegisterStepOne:UIStyling {
    func setupViews() {
        let frame = CGRect(x: 0, y: 0, width: superview.frame.width, height: 40)
        let titleView = TitleView(frame: frame)
        titleView.setupTitle(title:"Nieuw wachtwoord")
        self.superview.addSubview(titleView)
        setupConstraints()
    }
    
    func setupConstraints() {
        print("constraints")
    }

}
