//
//  UIFont+Extension.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/14/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//

import Foundation
import UIKit
//FIXME: We need to change font family here
public enum FontStyle: String {
    static let fontFamilyName = "HelveticaNeue"
    
    case light          = "Light"
    case regular        = "Regular"
    case medium         = "Medium"
    case bold           = "Bold"
    
    var name: String {
        switch self {
        case .regular:
            return FontStyle.fontFamilyName
        case .light, .medium, .bold:
            return "\(FontStyle.fontFamilyName)-\(rawValue)"
        }
    }
}

extension UIFont {
    static func custom(type: FontStyle, size: CGFloat) -> UIFont {
        return UIFont(name: type.name, size: size) ?? .systemFont(ofSize: size)
    }
}
